package com.msalihbindak.bitcointracker;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView priceText; // Initialize Bitcoin Prices Texts.
    Handler  handler;
    Runnable runnable;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();


        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        priceText = findViewById(R.id.priceText);   // Connect interface to variable




        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                BTC();
                handler.postDelayed(this,5000);
            }
        };
        handler.post(runnable);



    }


    public void BTC(){
        DownloadData downloadData = new DownloadData();   // Create downloadData Object.
        try {
            //String USER_INPUT = editText.getText().toString(); // Taking user input.
            String url = "https://www.btcturk.com/api/ticker";   // JSON URL
            downloadData.execute(url);                    // Start to take data from URL
        }catch (Exception e){
            e.printStackTrace();
        }
    }


private class DownloadData extends AsyncTask<String,Void,String> { // Download data class



    @Override
    protected String doInBackground(String... strings) {           // Download Data

        String result = "";                                        // Data in string
        URL url;                                                   // URL to Connect
        HttpURLConnection httpURLConnection;                       // Connection Object

        try{
            // URLye bağlan +  data çekme
            url = new URL(strings[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            int data = inputStreamReader.read();

            while (data > 0){
                char character =  (char) data;
                result += character;
                data=inputStreamReader.read();
            }

            return  result;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        // END URLye bağlan + data çekme

    }


    @Override
    protected void onPostExecute(String s) { // Process that will execute after download the data...

        //System.out.println("My data is: "+s); // Uygulamamız çalışıyor mu diye çektiğimiz veriyi yazdırdık. artık ihtiyaç yok. direk işlemeye geçiyoruz.
        super.onPostExecute(s);
        try{


            JSONArray jsonArray = new JSONArray(s);  // If data type is JsonArray, Create JsonArray
            JSONObject btctlx = jsonArray.getJSONObject(0); //1  BTC KAÇ TL   // Implement Json object from JsonArray
            //JSONObject ethbtc = jsonArray.getJSONObject(1); //1  ETH KAÇ BTC
            //JSONObject ethtlx = jsonArray.getJSONObject(2); //1  ETH KAÇ TL
            //JSONObject xrptlx = jsonArray.getJSONObject(3); //1  XRP KAÇ TL


            String btc = btctlx.getString("last");                           // Get the string data from Json object
            priceText.setText(btc+" TL");                                          // Update the text with new value.
            //System.out.println("1 Bitcoin "+last+" TL");


        }catch (Exception e){
            e.printStackTrace();
        }


    }


}

}